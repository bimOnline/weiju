package com.huixi.microspur.commons.enums;

/**
 * 删除标识枚举类
 */
public enum DelFlagEnum {
    NORMAL("N"),
    DEL("Y");

    private String value;

    DelFlagEnum(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
