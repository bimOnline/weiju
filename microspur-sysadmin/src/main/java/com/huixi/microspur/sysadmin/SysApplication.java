package com.huixi.microspur.sysadmin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动器
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 **/
@SpringBootApplication
@MapperScan("com.huixi.microspur.sysadmin.mapper")
public class SysApplication {
    public static void main(String[] args) {
        SpringApplication.run(SysApplication.class, args);
    }
}
