package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.entity.SysLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
public interface SysLogService extends IService<SysLog> {

}
