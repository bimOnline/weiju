package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.entity.SysMenu;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
