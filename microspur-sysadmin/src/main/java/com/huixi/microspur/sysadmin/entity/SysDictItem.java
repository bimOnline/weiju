package com.huixi.microspur.sysadmin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 字典子表
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_dict_item")
@ApiModel(value="SysDictItem对象", description="字典子表")
public class SysDictItem implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId("id")
    private String id;

    @ApiModelProperty(value = "序号")
    @TableField("sequence")
    private Integer sequence;

    @ApiModelProperty(value = "值")
    @TableField("value")
    private String value;

    @ApiModelProperty(value = "描述")
    @TableField("description")
    private String description;

    @ApiModelProperty(value = "字典id外检")
    @TableField("type_id")
    private String typeId;

    @TableField("create_by")
    private String createBy;

    @TableField("create_date")
    private LocalDateTime createDate;

    @TableField("update_by")
    private String updateBy;

    @TableField("update_date")
    private LocalDateTime updateDate;

    @ApiModelProperty(value = "删除标识")
    @TableField("del_flag")
    private String delFlag;


}
