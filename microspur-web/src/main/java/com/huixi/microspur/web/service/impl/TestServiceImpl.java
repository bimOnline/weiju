package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.Test;
import com.huixi.microspur.web.mapper.TestMapper;
import com.huixi.microspur.web.service.TestService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {

}
