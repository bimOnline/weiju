package com.huixi.microspur.web.entity.VO;

import com.huixi.microspur.commons.page.PageQuery;
import com.huixi.microspur.web.entity.appeal.WjAppealComment;
import lombok.Data;

/**
 *  诉求评论 分页VO类
 * @Author 叶秋
 * @Date 2020/4/13 22:45
 * @param
 * @return
 **/
@Data
public class WjAppealCommentPageVO {


    private WjAppealComment wjAppealComment;

    private PageQuery pageQuery;


}
