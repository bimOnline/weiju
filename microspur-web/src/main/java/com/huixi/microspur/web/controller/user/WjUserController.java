package com.huixi.microspur.web.controller.user;


import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.enums.ErrorCodeEnum;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.entity.Test;
import com.huixi.microspur.web.entity.user.EncryptionUserInfoVO;
import com.huixi.microspur.web.entity.user.WjUser;
import com.huixi.microspur.web.entity.user.WjUserWx;
import com.huixi.microspur.web.entity.user.WxSesssionKeyDTO;
import com.huixi.microspur.web.mapper.WjUserMapper;
import com.huixi.microspur.web.service.TestService;
import com.huixi.microspur.web.service.WjUserService;
import com.huixi.microspur.web.service.WjUserWxService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjUser")
@Slf4j
@Api(tags = "用户模块的接口")
public class WjUserController extends BaseController {


    @Autowired
    private WjUserService wjUserService;

    @Autowired
    private WjUserWxService wjUserWxService;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    TestService testService;

    @ApiOperation("测试查询方法")
    @GetMapping("/testOne")
    public Wrapper test(){

        Test byId = testService.getById("1");
        System.out.println("GG");
        return WrapMapper.ok(byId);


    }



    /**
     * 用于授权接口； 获取用户的code，获取其openID 和 个人的简单信息
     *  通过调用接口（如 wx.getUserInfo）获取数据时，接口会同时返回 rawData、signature，其中 signature = sha1( rawData + session_key )
     * 开发者将 signature、rawData 发送到开发者服务器进行校验。服务器利用用户对应的 session_key 使用相同的算法计算出签名 signature2 ，比对 signature 与 signature2 即可校验数据的完整性。
     *
     *@author: 李辉
     *@date: 2019/11/20 23:32
     *@return:
     */
    @PostMapping("/userAuthorization")
    @ApiOperation(value = "用于授权接口； 获取用户的code，获取其openID 和 个人的简单信息")
    public Wrapper userAuthorization(@RequestBody EncryptionUserInfoVO encryptionUserInfoVO) {

        String object = redisTemplate.opsForValue().get(encryptionUserInfoVO.getSessionKeyToUuid()).toString();
        WxSesssionKeyDTO wxSesssionKeyDTO = JSON.parseObject(object, WxSesssionKeyDTO.class);

        String sha1 =
                SecureUtil.sha1(encryptionUserInfoVO.getRawData() + wxSesssionKeyDTO.getSession_key());

        // 参数校验不一致
        if(!sha1.equals(encryptionUserInfoVO.getSignature())) {
            return WrapMapper.error(ErrorCodeEnum.USER003.msg());
        }

        // 返回加密的数据
        WjUser encryptionUserInfo = wjUserService.getEncryptionUserInfo(encryptionUserInfoVO.getEncryptedData(),
                wxSesssionKeyDTO.getSession_key(),
                encryptionUserInfoVO.getIv());




        return WrapMapper.ok(encryptionUserInfo);
    }





    /**
     * 获取用户的code，拿到微信后台去换取 session_key，存到redis 中，并返回UUID 给前端。
     *@author: 李辉
     *@date: 2019/11/21 0:00
     *@param code 用户code
     *@return:
     */
    @GetMapping("getUserCode/{code}")
    @ApiOperation(value = "获取用户的code，拿到微信后台去换取 session_key")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "用户的code", required = true, dataType = "String")
    })
    public Wrapper getUserCode(@PathVariable("code") String code) {


        // 用户 code 为空
        if(StrUtil.isEmpty(code)) {
            log.error(ErrorCodeEnum.USER001.msg());
            return WrapMapper.error(ErrorCodeEnum.USER001.msg());
        }


        String wxSessionKey = null;
        try {
            wxSessionKey = wjUserService.getWxSessionKey(code);
        } catch (Exception e) {
            // session_key 获取失败
            log.error(ErrorCodeEnum.USER002.msg() + "|" + e.getMessage());
            return WrapMapper.error(ErrorCodeEnum.USER002.msg());
        }


        return WrapMapper.ok(wxSessionKey);
    }



    /**
     *  检测用户登录情况 授权过就直接返还信息给他 | 反之就给null
     * @Author 李辉
     * @Date 2019/11/24 21:04
     * @param code
     * @return com.huixi.weiju.exception.util.CommonResult
     **/
    @PostMapping("/detectionUserAuthorization")
    @ApiOperation(value = "获取用户的code，拿到微信后台去换取 session_key")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "用户的code", required = true, dataType = "String")
    })
    public Wrapper detectionUserAuthorization(@RequestBody String code) throws Exception {

        String code1 = (String) JSON.parseObject(code).get("code");
        System.out.println(code1);

        WxSesssionKeyDTO wxSesssionKeyDTO = wjUserService.detectionUserAuthorization(code1);
        QueryWrapper<WjUserWx> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("wx_open_id", wxSesssionKeyDTO.getOpenid());
        WjUserWx one = wjUserWxService.getOne(objectQueryWrapper);

        // 检查数据库中是否有数据
        if(one == null) {
            return null;
        }

        // 有值就返回
        WjUser wjUser = wjUserService.getById(one.getUserId());


        return WrapMapper.ok(wjUser);



    }


}

