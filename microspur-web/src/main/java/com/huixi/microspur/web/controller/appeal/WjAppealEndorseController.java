package com.huixi.microspur.web.controller.appeal;

import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.enums.ErrorCodeEnum;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.entity.appeal.WjAppealEndorse;
import com.huixi.microspur.web.service.WjAppealEndorseService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *  诉求点赞 模块
 * @Author 叶秋
 * @Date 2020/3/18 22:17
 * @param
 * @return
 **/
@RestController
@RequestMapping("/wjAppealEndorse")
@Api(tags = "诉求点赞的接口")
public class WjAppealEndorseController extends BaseController {


    @Autowired
    private WjAppealEndorseService wjAppealEndorseService;

    /**
     *  为诉求点赞
     * @Author 叶秋
     * @Date 2020/3/18 22:24
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/appealToEndorse")
    public Wrapper appealToEndorse(@RequestBody WjAppealEndorse wjAppealEndorse){

        boolean save = wjAppealEndorseService.save(wjAppealEndorse);

        if(save){
            return WrapMapper.ok();
        }

        return WrapMapper.error(ErrorCodeEnum.APPEALENDORSE001.msg());


    }

    /**
     *  取消帖子的点赞
     * @Author 叶秋
     * @Date 2020/3/20 0:29
     * @param wjAppealEndorse 接受的类
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @DeleteMapping("/cancelEndorse")
    public Wrapper cancelEndorse(@RequestBody WjAppealEndorse wjAppealEndorse){

        Boolean aBoolean = wjAppealEndorseService.cancelEndorse(wjAppealEndorse);
        if(!aBoolean){
            return WrapMapper.error(ErrorCodeEnum.APPEALCANCELENDORSE002.msg());
        }

        return WrapMapper.ok();
    }





}
